/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.repository;

import fit5042.tutex.repository.entities.Property;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO Exercise 1.3 Step 2 Complete this class.
 * 
 * This class implements the PropertyRepository class. You will need to add the keyword
 * "implements" PropertyRepository. 
 * 
 * @author Ziyi Zhu
 */



 public class SimplePropertyRepositoryImpl implements PropertyRepository {
	private final ArrayList<Property> propertiesOfAgency;

    public SimplePropertyRepositoryImpl() {
    	this.propertiesOfAgency = new ArrayList<>();
        
    }

	public SimplePropertyRepositoryImpl(ArrayList<Property> propertiesOfAgency) {
		super();
		this.propertiesOfAgency = propertiesOfAgency;
	}
	
	

	@Override
	public void addProperty(Property property) throws Exception {
		// TODO Auto-generated method stub
		if ((!propertiesOfAgency.contains(property)) && (searchPropertyById(property.getId()) == null))
            this.propertiesOfAgency.add(property);
		
	}

	@Override
	public Property searchPropertyById(int id) throws Exception {
		// TODO Auto-generated method stub
		for(Property property:propertiesOfAgency) {
			if(id == property.getId()) {
				return property;
			}
			
		}
		return null;
	}

	@Override
	public List<Property> getAllProperties() throws Exception {
		// TODO Auto-generated method stub
		List<Property> pp = new ArrayList<>();
		for(Property p:this.propertiesOfAgency) {
			pp.add(p);
		}
		return pp;
	}
    
}

